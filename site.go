package collector

import (
	"math/rand"
	"net/http"
	"reflect"
	"strconv"
	"strings"
	"time"
)

// SiteData a site data, can be a Post, Article, Video etc
type SiteData struct {
	Date       time.Time  `json:"Date"`
	Title      string     `json:"Title"`
	Identifier string     `json:"Identifier"`
	Tags       []string   `json:"Tags,omitempty"`
	Categories []string   `json:"Categories,omitempty"`
	Body       string     `json:"Body,omitempty"`
	Cover      string     `json:"Cover,omitempty"`
	Rating     float32    `json:"Rating,omitempty"`
	Related    []SiteData `json:"Related,omitempty"`
}

// Empty check if SiteData is empty
func (s *SiteData) Empty() bool {
	return reflect.DeepEqual(*s, SiteData{})
}

// Keywords generate Keywords for SiteData
func (s *SiteData) Keywords() []string {
	var kw []string

	kw = append(kw, s.Title)
	kw = append(kw, s.Categories...)
	kw = append(kw, s.Tags...)

	return kw
}

func (s *SiteData) RelatedKeywords() string {
	kw := s.Keywords()

	// remove title from keywords
	if len(kw) > 1 {
		kw = kw[1:]
	}

	// shuffle and pick 3 keywords
	if len(kw) > 3 {
		rand.Seed(time.Now().UnixNano())
		rand.Shuffle(len(kw), func(i, j int) {
			kw[i], kw[j] = kw[j], kw[i]
		})
		kw = kw[1:4]
	}

	return strings.Join(kw, " ")
}

type ProcessByte func([]byte, chan SiteData, chan error)
type PageCallback func(cp, mp int) (int, int)

// SiteSource a source for Site
type SiteSource struct {
	URL        string
	IsPaginate bool
	MaxPage    int
	ProcessByte
	PageCallback
}

func (ss SiteSource) Empty() bool {
	return len(ss.URL) == 0
}

func (ss SiteSource) GetUrl(p int) string {
	if !ss.IsPaginate {
		return ss.URL
	}

	return strings.Replace(ss.URL, ":PAGE:", strconv.Itoa(p), 1)
}

// Site a site!, usually site has Feed, Detail and Search source
type Site interface {
	Name() string
	ID() string
	Feed() SiteSource
	Detail() SiteSource
	Search() SiteSource
	HttpClient() *http.Client
}
