package collector

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"sync"
)

type Field uint8

const (
	Feed Field = iota
	Search
	Detail
)

type Transformer func(items []SiteData) []SiteData

type App struct {
	sites        map[string]Site
	handler      ErrorHandler
	transformers []Transformer
}

// Empty check registered sites
func (app *App) Empty() bool {
	return len(app.sites) == 0
}

// AddSite register a Site to App, quietly ignore if already registered
func (app *App) AddSite(site Site) {
	key := site.ID()
	if _, found := app.sites[key]; !found {
		app.sites[key] = site
	}
}

// AddTransformer register a Transformer
func (app *App) AddTransformer(t Transformer) {
	app.transformers = append(app.transformers, t)
}

// SetLogger set app error handler
func (app *App) SetErrorHandler(h ErrorHandler) {
	app.handler = h
}

// Feed get feed items from all sites
func (app *App) Feed() []SiteData {
	return app.callSource(Feed, nil)
}

// Search search through all sites
func (app *App) Search(keyword string) []SiteData {
	return app.callSource(Search, keyword)
}

// Detail get detail data
func (app *App) Detail(identifier string, withRelated bool) (item SiteData) {
	items := app.callSource(Detail, identifier)

	if len(items) > 0 {
		item = items[0]
	}

	if !item.Empty() && withRelated {
		if len(item.Related) > 0 {
			return
		}

		kw := url.QueryEscape(item.RelatedKeywords())
		result := clone(app).Search(kw)

		for i := range result {
			if result[i].Identifier != item.Identifier {
				item.Related = append(item.Related, result[i])
			}
		}
	}

	return
}

func (app *App) callSource(field Field, data interface{}) (items []SiteData) {
	// wait group
	wg := new(sync.WaitGroup)
	// site data chan
	csd := make(chan SiteData)
	// done chan
	dn := make(chan bool, 1)
	// error chan
	cer := make(chan error)
	// found mark
	f := false

	go func() {
		for sd := range csd {
			items = append(items, sd)
		}
		dn <- true
	}()

	go func() {
		for e := range cer {
			app.handler(e)
		}
		dn <- true
	}()

	for i := range app.sites {
		var source SiteSource

		switch field {
		case Feed:
			source = app.sites[i].Feed()
		case Search:
			source = app.sites[i].Search()
		case Detail:
			if f {
				continue
			}

			id := ExtractID(data)
			if app.sites[i].ID() == id.SiteID {
				f = true
				source = app.sites[i].Detail()
				data = id.OriginalSiteID
			}
		}

		if source.Empty() {
			continue
		}

		wg.Add(1)
		go app.capture(app.sites[i].HttpClient(), source, wg, csd, data, cer)
	}

	wg.Wait()
	close(csd)

	<-dn
	close(dn)

	for _, transformer := range app.transformers {
		items = transformer(items)
	}

	return
}

func (app *App) capture(c *http.Client, source SiteSource, w *sync.WaitGroup, csd chan SiteData, data interface{}, cer chan error) {
	defer w.Done()

	// current page
	var cp = 1
	// max page
	var mp = 1

	if source.IsPaginate && source.MaxPage >= 1 {
		mp = source.MaxPage
	}

	if nil != source.PageCallback {
		cp, mp = source.PageCallback(cp, mp)
	}

	// new wait group
	nwg := new(sync.WaitGroup)

	for cp <= mp {
		nwg.Add(1)
		go app.processPage(c, source, cp, data, csd, nwg, cer)
		cp++
	}

	nwg.Wait()
}

func (app *App) processPage(c *http.Client, s SiteSource, cp int, d interface{}, csd chan SiteData, wg *sync.WaitGroup, cer chan error) {
	defer wg.Done()

	u := PrepareURL(s.GetUrl(cp), d)

	response, err := c.Get(PrepareURL(s.GetUrl(cp), d))

	if err != nil {
		cer <- fmt.Errorf("unable to read the response body(%v): %v", u, err)
		return
	}

	byteResponse, err := ioutil.ReadAll(response.Body)
	if err != nil {
		cer <- fmt.Errorf("unable to read the response body(%v): %v", u, err)
		return
	}

	s.ProcessByte(byteResponse, csd, cer)
}

// New create app with default configuration
func New() *App {
	return &App{
		handler: DefaultErrorHandler,
		sites:   make(map[string]Site),
	}
}

func clone(app *App) *App {
	return &App{
		sites:        app.sites,
		handler:      app.handler,
		transformers: app.transformers,
	}
}
