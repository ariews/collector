package collector

import (
	"fmt"
	"strings"
)

type Sid struct {
	SiteID         string
	OriginalSiteID string
}

func ExtractID(data interface{}) Sid {
	info := strings.Split(fmt.Sprintf("%v", data), ":")

	return Sid{
		SiteID:         info[0],
		OriginalSiteID: info[1],
	}
}

func MakeID(sid string, oid interface{}) string {
	return fmt.Sprintf("%s:%v", sid, oid)
}

func PrepareURL(url string, data interface{}) string {
	switch d := data.(type) {
	case nil:
		return url
	default:
		return fmt.Sprintf(url, d)
	}
}
