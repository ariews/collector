package collector

import "log"

type ErrorHandler func(err error)

func DefaultErrorHandler(err error) {
	log.Printf(err.Error())
}
